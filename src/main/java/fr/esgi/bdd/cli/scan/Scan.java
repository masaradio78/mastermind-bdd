package fr.esgi.bdd.cli.scan;

public interface Scan {

    Integer inputInt();

    String inputLine();
}
