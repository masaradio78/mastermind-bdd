package fr.esgi.bdd.cli.scan;

import java.util.Scanner;

public class ScanImpl implements Scan {
    private final Scanner scanner;

    public ScanImpl() {
        scanner = new Scanner(System.in);
    }

    @Override
    public Integer inputInt() {
        return this.scanner.nextInt();
    }

    @Override
    public String inputLine() {
        return this.scanner.nextLine();
    }
}
