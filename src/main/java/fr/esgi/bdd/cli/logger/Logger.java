package fr.esgi.bdd.cli.logger;

public interface Logger {
    public void out(String message);
}
