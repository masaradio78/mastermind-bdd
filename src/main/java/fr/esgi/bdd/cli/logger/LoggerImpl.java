package fr.esgi.bdd.cli.logger;

public class LoggerImpl implements Logger {
    @Override
    public void out(String message) {
        System.out.println(message);
    }
}
