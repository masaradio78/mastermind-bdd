package fr.esgi.bdd.cli;

public enum Level {
    EASY(2),
    MEDIUM(4),
    DIFFICULT(8);

    private final Integer level;

    Level(Integer level) {
        this.level = level;
    }

    public Integer getNumberSecretColors() {
        return this.level;
    }
}
