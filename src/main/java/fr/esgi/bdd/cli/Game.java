package fr.esgi.bdd.cli;

import fr.esgi.bdd.cli.logger.Logger;
import fr.esgi.bdd.cli.scan.Scan;
import fr.esgi.bdd.core.IncorrectGivenColorsSizeException;
import fr.esgi.bdd.core.Mastermind;
import fr.esgi.bdd.core.NotAvailableColorException;

import java.util.*;
import java.util.stream.Collectors;

public class Game {
    private final Mastermind mastermind;
    private final Scan scan;
    private final Logger logger;

    public Game(Mastermind mastermind, Scan scan, Logger logger) {
        this.mastermind = mastermind;
        this.scan = scan;
        this.logger = logger;
    }

    public void execute() {
        ArrayList<String> availableColors = createAvailableColors();
        mastermind.setAvailableColors(availableColors);

        logger.out("Welcome to CLI Mastermind\n");
        var numberTurns = getAppropriateIntValue(
                "Enter the number of turns between 5 and 20",
                5,
                20
        );

        prepareSecretList(mastermind);

        startGame(numberTurns);
    }

    private void startGame(Integer numberTurns) {
        logger.out("\nThe secret colors are set. Now figure out those colors\n");
        while (numberTurns > 0) {
            logger.out("Remaining " + (numberTurns <= 1 ? "turn" : "turns") + " : " + numberTurns);
            logger.out("Choose " + mastermind.getSecretColorsSize() + " colors from the list : " + mastermind.getAvailableColors());
            var guessingColors = Arrays.stream(scan.inputLine().split(" ")).collect(Collectors.toList());

            try {
                var result = mastermind.start(guessingColors);
                if (result.getNumberWellPlacedColors() == mastermind.getSecretColorsSize()) {
                    logger.out("Congrats ! You figure out the secret colors that is " + guessingColors.toString());
                    return;
                }
                logger.out("Number of well placed colors : " + result.getNumberWellPlacedColors());
                logger.out("Number of misplaced colors : " + result.getNumberMisplacedColors());
                numberTurns--;
            } catch (IncorrectGivenColorsSizeException e) {
                logger.out("You have to give the appropriate number of colors. Try again.");
            } catch (NotAvailableColorException e) {
                logger.out("You enter at least one color that is not available. Try again.");
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger.out("");
        }
        logger.out("Sorry you lose. The secret list is : " + mastermind.getSecretColors());
        logger.out("Execute the program again to replay the game.");
    }

    private Integer getAppropriateIntValue(String message, Integer min, Integer max) {
        while (true) {
            logger.out(message);
            try {
                var input = scan.inputInt();
                scan.inputLine();
                if (input >= min && input <= max) {
                    return input;
                }
            } catch (InputMismatchException ignored) {
                scan.inputLine();
            }
            logger.out("Invalid integer input, try again");
        }
    }

    private void prepareSecretList(Mastermind mastermind) {
        Map<Integer, Level> mapLevels = getMapLevels();
        var isChosenLevelCorrectInvalid = true;
        var chosenInputLevel = -1;

        while (isChosenLevelCorrectInvalid) {
            chosenInputLevel = getAppropriateIntValue(
                    "Choose the level of game : 1. easy("
                            + Level.EASY.getNumberSecretColors() + " secret colors), 2.medium("
                            + Level.MEDIUM.getNumberSecretColors() + " secret colors), 3.difficulty("
                            + Level.DIFFICULT.getNumberSecretColors() + " secret colors)",
                    1,
                    3
            );
            isChosenLevelCorrectInvalid = !mapLevels.containsKey(chosenInputLevel);
            if (isChosenLevelCorrectInvalid) {
                logger.out("The chosen game is not correct");
            }
        }
        var secretColors = new ArrayList<String>();
        var chosenLevel = mapLevels.get(chosenInputLevel);
        var availableColors = mastermind.getAvailableColors();
        for (int i = 0; i < chosenLevel.getNumberSecretColors(); i++) {
            var randomInt = new Random().nextInt(availableColors.size());
            secretColors.add(availableColors.get(randomInt));
        }
        mastermind.setSecretStateColors(secretColors);
    }

    private Map<Integer, Level> getMapLevels() {
        Map<Integer, Level> mapLevels = new HashMap<>();
        mapLevels.put(1, Level.EASY);
        mapLevels.put(2, Level.MEDIUM);
        mapLevels.put(3, Level.DIFFICULT);
        return mapLevels;
    }

    private ArrayList<String> createAvailableColors() {
        var availableColors = new ArrayList<String>();
        availableColors.add("black");
        availableColors.add("white");
        availableColors.add("red");
        availableColors.add("green");
        availableColors.add("blue");
        availableColors.add("cyan");
        availableColors.add("orange");
        availableColors.add("purple");
        return availableColors;
    }
}
