package fr.esgi.bdd.cli;

import fr.esgi.bdd.cli.logger.Logger;
import fr.esgi.bdd.cli.logger.LoggerImpl;
import fr.esgi.bdd.cli.scan.Scan;
import fr.esgi.bdd.cli.scan.ScanImpl;
import fr.esgi.bdd.core.Mastermind;

public class Application {
    public static void main(String[] args) {
        Logger logger = new LoggerImpl();
        Scan scan = new ScanImpl();

        var mastermind = new Mastermind();

        Game game = new Game(mastermind, scan, logger);
        game.execute();
    }
}
