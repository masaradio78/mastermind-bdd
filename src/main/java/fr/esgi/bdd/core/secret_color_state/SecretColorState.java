package fr.esgi.bdd.core.secret_color_state;

import java.util.Objects;

public class SecretColorState {
    private final String color;
    private Boolean isChecked;

    public SecretColorState(String color) {
        this.color = color;
        isChecked = false;
    }

    public String getColor() {
        return color;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SecretColorState that = (SecretColorState) o;
        return Objects.equals(color, that.color) && Objects.equals(isChecked, that.isChecked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, isChecked);
    }

    @Override
    public String toString() {
        return "SecretColorState{" +
                "color='" + color + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
