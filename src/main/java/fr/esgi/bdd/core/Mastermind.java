package fr.esgi.bdd.core;

import fr.esgi.bdd.core.secret_color_state.SecretColorState;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Mastermind {
    private List<String> availableColors = new ArrayList<>();
    private List<SecretColorState> secretStateColors;

    public List<String> getAvailableColors() {
        return availableColors;
    }

    public Integer getSecretColorsSize() {
        return secretStateColors.size();
    }

    public List<String> getSecretColors() {
        return secretStateColors.stream()
                .map(SecretColorState::getColor)
                .collect(Collectors.toList());
    }

    public void setAvailableColors(ArrayList<String> availableColors) {
        this.availableColors = availableColors;
    }

    public void setSecretStateColors(List<String> secretStateColors) {
        this.secretStateColors = secretStateColors.stream()
                .map(SecretColorState::new)
                .collect(Collectors.toList());
    }

    public Result start(List<String> givenColors) throws Exception {
        resetCheckedSecretStateColorsToFalse();
        checkSecretAndGivenColors(givenColors);

        return searchingWellPlacedAndMisplacedColors(givenColors);
    }

    private void checkSecretAndGivenColors(List<String> givenColors) throws IncorrectSecretColorsSizeException, IncorrectGivenColorsSizeException, NotAvailableColorException {
        if (secretStateColors.size() <= 0) {
            throw new IncorrectSecretColorsSizeException();
        }
        if (secretStateColors.size() != givenColors.size()) {
            throw new IncorrectGivenColorsSizeException();
        }
        if (givenColors.stream().anyMatch(color -> !availableColors.contains(color))) {
            throw new NotAvailableColorException();
        }
    }

    private void resetCheckedSecretStateColorsToFalse() {
        this.secretStateColors
                .forEach(secretColorState -> secretColorState.setChecked(false));
    }

    private Result searchingWellPlacedAndMisplacedColors(List<String> givenColors) {
        Result result = new Result();

        givenColors = searchWellPlacedColorsAndReturnFilteredGivenColors(givenColors, result);
        if (result.getNumberWellPlacedColors() < secretStateColors.size()) {
            searchMisplacedColors(givenColors, result);
        }

        return result;
    }

    private List<String> searchWellPlacedColorsAndReturnFilteredGivenColors(List<String> givenColors, Result result) {
        List<String> newGivenColors = new ArrayList<>();
        IntStream.range(0, givenColors.size())
                .forEach(i -> {
                    if (areColorsEqual(givenColors.get(i), secretStateColors.get(i).getColor())) {
                        result.setNumberWellPlacedColors(result.getNumberWellPlacedColors() + 1);
                        secretStateColors.get(i).setChecked(true);
                        return;
                    }
                    newGivenColors.add(givenColors.get(i));
                });

        return newGivenColors;
    }

    private boolean areColorsEqual(String currentColor, String otherColor) {
        return currentColor.equals(otherColor);
    }

    private void searchMisplacedColors(List<String> givenColors, Result result) {
        givenColors.forEach(givenColor -> {
            this.secretStateColors.stream()
                    .filter(isSecretColorSameAndNotGetChecked(givenColor))
                    .findFirst().ifPresent(findColorNotChecked -> {
                findColorNotChecked.setChecked(true);
                result.setNumberMisplacedColors(result.getNumberMisplacedColors() + 1);
            });
        });
    }

    private Predicate<SecretColorState> isSecretColorSameAndNotGetChecked(String givenColor) {
        return secretColorState -> !secretColorState.getChecked()
                && areColorsEqual(givenColor, secretColorState.getColor());
    }
}
