package fr.esgi.bdd.core.available_colors;

import java.util.List;

public class AvailableColors {
    private final List<String> colors;

    public AvailableColors(final List<String> colors) {
        this.colors = colors;
    }

    @Override
    public String toString() {
        return "Colors{" +
                "colors=" + colors +
                '}';
    }
}
