package fr.esgi.bdd.core;

import java.util.Objects;

public class Result {
    private int numberWellPlacedColors;
    private int numberMisplacedColors;

    public Result() {
        this.numberWellPlacedColors = 0;
        this.numberMisplacedColors = 0;
    }

    public int getNumberWellPlacedColors() {
        return numberWellPlacedColors;
    }

    public void setNumberWellPlacedColors(int numberWellPlacedColors) {
        this.numberWellPlacedColors = numberWellPlacedColors;
    }

    public int getNumberMisplacedColors() {
        return numberMisplacedColors;
    }

    public void setNumberMisplacedColors(int numberMisplacedColors) {
        this.numberMisplacedColors = numberMisplacedColors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return numberWellPlacedColors == result.numberWellPlacedColors && numberMisplacedColors == result.numberMisplacedColors;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberWellPlacedColors, numberMisplacedColors);
    }

    @Override
    public String toString() {
        return "Result{" +
                "numberWellPlacedColors=" + numberWellPlacedColors +
                ", numberMisplacedColors=" + numberMisplacedColors +
                '}';
    }
}
