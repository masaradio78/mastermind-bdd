package fr.esgi.bdd.features.standard;

import fr.esgi.bdd.core.available_colors.AvailableColors;
import fr.esgi.bdd.features.World;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class AvailableColorsStep {

    private final World world;
    private Scenario scenario;

    public AvailableColorsStep(final World world) {
        this.world = world;
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Before
    @Given("default available colors are used")
    public void defaultAvailableColorsAreUsed() {
        createDefaultAvailableColors();
    }

    public void createDefaultAvailableColors() {
        var availableColors = new ArrayList<String>();
        availableColors.add("blue");
        availableColors.add("red");
        availableColors.add("gray");
        availableColors.add("green");
        availableColors.add("yellow");
        availableColors.add("purple");
        availableColors.add("black");
        availableColors.add("white");
        availableColors.add("cyan");
        availableColors.add("pink");
        world.mastermind().setAvailableColors(availableColors);
    }

    @Then("the available colors should be at least:$")
    public void theAvailableColorsShouldBeAtLeast(List<String> colors) {
        Assertions.assertThat(world.mastermind().getAvailableColors()).containsAll(colors);
    }

    @DataTableType
    public AvailableColors availableColors(List<String> colors) {
        return new AvailableColors(colors);
    }
}
