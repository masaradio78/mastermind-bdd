package fr.esgi.bdd.features;

import fr.esgi.bdd.core.Mastermind;
import fr.esgi.bdd.core.Result;
import fr.esgi.bdd.core.available_colors.AvailableColors;

import java.util.List;

public class World {
    private final Mastermind mastermind;
    private Result result;
    private List<String> secretColors;
    private List<String> givenColors;
    private AvailableColors availableColorsProperty;

    public World(final Mastermind mastermind) {
        this.mastermind = mastermind;
    }

    public Mastermind mastermind() {
        return mastermind;
    }

    public Mastermind getMastermind() {
        return mastermind;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public List<String> getSecretColors() {
        return secretColors;
    }

    public void setSecretColors(List<String> secretColors) {
        this.secretColors = secretColors;
    }

    public List<String> getGivenColors() {
        return givenColors;
    }

    public void setGivenColors(List<String> givenColors) {
        this.givenColors = givenColors;
    }

    public AvailableColors getAvailableColorsProperty() {
        return availableColorsProperty;
    }

    public void setAvailableColorsProperty(AvailableColors availableColorsProperty) {
        this.availableColorsProperty = availableColorsProperty;
    }
}
