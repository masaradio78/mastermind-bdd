package fr.esgi.bdd.features;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"fr.esgi.bdd"}
        ,tags = "not @wip"
)
public class RunCucumberTest {

}
