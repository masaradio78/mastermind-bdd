package fr.esgi.bdd.features.game;

import fr.esgi.bdd.core.IncorrectGivenColorsSizeException;
import fr.esgi.bdd.core.IncorrectSecretColorsSizeException;
import fr.esgi.bdd.core.NotAvailableColorException;
import fr.esgi.bdd.core.Result;
import fr.esgi.bdd.features.World;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class GameStep {

    private final World world;

    public GameStep(final World world) {
        this.world = world;
    }

    @Given("^the secret list of colors$")
    public void theSecretListOfColors(List<String> secretColors) {
        world.setSecretColors(secretColors);
        world.mastermind().setSecretStateColors(secretColors);
    }

    @When("^the guessing list of colors$")
    public void theGuessingListOfColors(List<String> givenColors) {
        world.setGivenColors(givenColors);
    }

    @Then("the game should indicate <{int}> misplaced color")
    public void theGameShouldIndicateMisplacedColor(int oneMisplacedColor) throws Exception {
        Result result = world.getMastermind().start(world.getGivenColors());
        Assertions.assertThat(result.getNumberMisplacedColors()).isEqualTo(oneMisplacedColor);
    }

    @Then("the game should indicate <{int}> misplaced colors")
    public void theGameShouldIndicateMisplacedColors(int numberMisplacedColors) throws Exception {
        Result result = world.getMastermind().start(world.getGivenColors());
        Assertions.assertThat(result.getNumberMisplacedColors()).isEqualTo(numberMisplacedColors);
    }

    @Then("the game should indicate <{int}> well placed color")
    public void theGameShouldIndicateWellPlacedColor(int oneWellPlacedColor) throws Exception {
        Result result = world.getMastermind().start(world.getGivenColors());
        Assertions.assertThat(result.getNumberWellPlacedColors()).isEqualTo(oneWellPlacedColor);
    }

    @Then("the game should indicate <{int}> well placed colors")
    public void theGameShouldIndicateWellPlacedColors(int numberWellPlacedColors) throws Exception {
        Result result = world.getMastermind().start(world.getGivenColors());
        Assertions.assertThat(result.getNumberWellPlacedColors()).isEqualTo(numberWellPlacedColors);
    }

    @And("the game should indicate a problem of guessing color list length")
    public void theGameShouldIndicateAProblemOfGuessingColorListLength() {
        Assertions
                .assertThatThrownBy(() -> world.getMastermind().start(world.getGivenColors()))
                .isInstanceOf(IncorrectGivenColorsSizeException.class);
    }

    @Then("the game should indicate a problem of not available color present")
    public void theGameShouldIndicateAProblemOfNotAvailableColorPresent() {
        Assertions
                .assertThatThrownBy(() -> world.getMastermind().start(world.getGivenColors()))
                .isInstanceOf(NotAvailableColorException.class);
    }

    @Then("the game should indicate the size of secret colors to <{int}>")
    public void theGameShouldIndicateTheSizeOfSecretColorsTo(int expectedSize) {
        Integer size = world.mastermind().getSecretColorsSize();
        Assertions.assertThat(size).isEqualTo(expectedSize);
    }

    @Then("the game can't be able to indicate the size of secret colors")
    public void theGameCanTBeAbleToIndicateTheSizeOfSecretColors() {
        Assertions
                .assertThatThrownBy(() -> world.getMastermind().start(world.getGivenColors()))
                .isInstanceOf(IncorrectSecretColorsSizeException.class);
    }

    @When("the secret list of colors ,")
    public void theSecretListOfColors() {
        world.setSecretColors(new ArrayList<>());
        world.mastermind().setSecretStateColors(new ArrayList<>());
    }
}
