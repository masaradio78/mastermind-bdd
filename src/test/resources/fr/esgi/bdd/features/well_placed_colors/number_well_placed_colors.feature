Feature: Detecting well placed colors
  As a player of mastermind
  I want the game to return well placed colors of my colors choice
  So that I can suggest where my colors are rights

  Scenario: One well placed color
    Given the secret list of colors
      | red  |
      | blue |
    When the guessing list of colors
      | red   |
      | green |
    Then the game should indicate <1> well placed color

  Scenario: Zero well placed color
    Given the secret list of colors
      | red    |
      | gray   |
      | blue   |
      | yellow |
    When the guessing list of colors
      | purple |
      | green  |
      | cyan   |
      | black  |
    Then the game should indicate <0> well placed colors

  Scenario: Two well placed colors
    Given the secret list of colors
      | white  |
      | purple |
      | red    |
      | blue   |
    When the guessing list of colors
      | black  |
      | purple |
      | red    |
      | pink   |
    Then the game should indicate <2> well placed colors

  Scenario: All well placed colors
    Given the secret list of colors
      | purple |
      | green  |
      | cyan   |
      | black  |
      | white  |
      | green  |
      | red    |
      | blue   |
    When the guessing list of colors
      | purple |
      | green  |
      | cyan   |
      | black  |
      | white  |
      | green  |
      | red    |
      | blue   |
    Then the game should indicate <8> well placed colors