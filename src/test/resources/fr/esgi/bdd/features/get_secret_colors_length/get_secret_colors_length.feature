Feature: Get secret colors length
  As a player of mastermind
  I want the game to return the size of secret colors
  So that I can right number of guessing colors

  Scenario: One secret color
    When the secret list of colors
      | red |
    Then the game should indicate the size of secret colors to <1>

  Scenario: Few secret colors
    When the secret list of colors
      | red   |
      | blue  |
      | green |
      | white |
    Then the game should indicate the size of secret colors to <4>

  Scenario: No secret color
    When  the secret list of colors ,
    Then the game can't be able to indicate the size of secret colors
