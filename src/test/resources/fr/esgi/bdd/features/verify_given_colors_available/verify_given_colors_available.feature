Feature: Verify if given colors are available
  As a player of mastermind
  I want the game to inform well placed colors an misplaced
  Only when I give a guessing available colors by the game
  Otherwise the game should inform a problem of not available color present

  Scenario: Not able to inform when a color is not available
    Given the secret list of colors
      | red   |
      | green |
      | blue  |
      | pink  |
    When the guessing list of colors
      | notColor |
      | pink     |
      | blue     |
      | black    |
    Then the game should indicate a problem of not available color present
