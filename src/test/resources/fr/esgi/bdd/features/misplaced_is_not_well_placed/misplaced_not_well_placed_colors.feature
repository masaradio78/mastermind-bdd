Feature: Misplaced is not well placed colors
  As a player of mastermind
  I want the game to inform well placed colors that is not correct misplaced colors
  So that I can figure out how many correct but misplaced colors are in my colors

  Scenario: One well placed and one correct misplaced color
    Given the secret list of colors
      | red   |
      | green |
      | blue  |
    When the guessing list of colors
      | red   |
      | blue  |
      | black |
    Then the game should indicate <1> well placed color
    And the game should indicate <1> misplaced color

  Scenario: All well placed colors
    Given the secret list of colors
      | red   |
      | green |
      | blue  |
      | black |
    When the guessing list of colors
      | red   |
      | green |
      | blue  |
      | black |
    Then the game should indicate <4> well placed colors
    And the game should indicate <0> misplaced colors