Feature: Same colors secret list
  As a player of mastermind
  I want the game to inform well placed colors
  with secret list colors that contains few same colors

  Scenario: secret list contain only same colors
    Given the secret list of colors
      | red |
      | red |
      | red |
    When the guessing list of colors
      | red   |
      | red   |
      | green |
    Then the game should indicate <2> well placed colors
    And the game should indicate <0> misplaced colors

  Scenario: guessing list contain more same color that secret list and 0 well placed
    Given the secret list of colors
      | red   |
      | red   |
      | green |
    When the guessing list of colors
      | green |
      | green |
      | red   |
    Then the game should indicate <0> well placed colors
    And the game should indicate <2> misplaced colors

  Scenario: secret list contain only one color and guessing list have one well placed and 2 misplaced
    Given the secret list of colors
      | green |
      | green |
      | green |
    When the guessing list of colors
      | red   |
      | green |
      | red   |
    Then the game should indicate <1> well placed colors
    And the game should indicate <0> misplaced colors

  Scenario: guessing list contain only one color and only one well placed compare to secret list
    Given the secret list of colors
      | blue  |
      | green |
      | red   |
    When the guessing list of colors
      | blue |
      | blue |
      | blue |
    Then the game should indicate <1> well placed colors
    And the game should indicate <0> misplaced colors

  Scenario: guessing list and secret list same only one color
    Given the secret list of colors
      | pink |
      | pink |
      | pink |
      | pink |
    When the guessing list of colors
      | pink |
      | pink |
      | pink |
      | pink |
    Then the game should indicate <4> well placed colors
    And the game should indicate <0> misplaced colors

  Scenario: guessing list and secret list with same misplaced color not next to it and one well placed in middle
    Given the secret list of colors
      | pink   |
      | blue   |
      | yellow |
      | pink   |
      | black  |
    When the guessing list of colors
      | white  |
      | pink   |
      | yellow |
      | black  |
      | pink   |
    Then the game should indicate <1> well placed colors
    And the game should indicate <3> misplaced colors