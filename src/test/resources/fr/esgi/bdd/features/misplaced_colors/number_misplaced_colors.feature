Feature: Detecting correct but misplaced colors
  As a player of mastermind
  I want the game to return correct but misplaced colors of my colors choice
  So that I can suggest to move correct colors to be well placed

  Scenario: One misplaced colors
    Given the secret list of colors
      | red  |
      | blue |
    When the guessing list of colors
      | blue  |
      | green |
    Then the game should indicate <1> misplaced color

  Scenario: All misplaced colors
    Given the secret list of colors
      | red    |
      | gray   |
      | blue   |
      | yellow |
    When the guessing list of colors
      | gray   |
      | red    |
      | yellow |
      | blue   |
    Then the game should indicate <4> misplaced colors

  Scenario: Zero misplaced colors
    Given the secret list of colors
      | red  |
      | blue |
    When the guessing list of colors
      | green  |
      | purple |
    Then the game should indicate <0> misplaced colors