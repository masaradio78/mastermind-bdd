Feature: Verify number guessing and secret lists of colors
  As a player of mastermind
  I want the game to inform well placed colors and misplaced color
  Only when I give the same number of colors than the secret list of colors
  Otherwise the game should inform a problem of guessing color list length

  Scenario: Not able to inform when the number of colors of the secret list is lower than the guessing list
    Given the secret list of colors
      | red   |
      | green |
      | blue  |
    When the guessing list of colors
      | red   |
      | green |
    Then the game should indicate a problem of guessing color list length

  Scenario: Not able to inform when the number of colors of the secret list is greater than the guessing list
    Given the secret list of colors
      | red   |
      | green |
      | blue  |
    When the guessing list of colors
      | blue  |
      | white |
      | red   |
      | green |
    Then the game should indicate a problem of guessing color list length