Feature: default available colors definition

  Scenario: Default available colors
    Given default available colors are used
    Then the available colors should be at least:
      | blue   |
      | red    |
      | gray   |
      | green  |
      | yellow |
      | purple |
      | black  |
      | white  |
      | cyan   |
      | pink   |