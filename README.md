# mastermind-bdd

The mastermind can be play by cli.

##### Before to play the game
- You need to install the JDK 11
- You need to install mavan

##### To build the application
`
mvn clean package
`

##### To play the game
`
java -jar target/mastermind-bdd-1.0-SNAPSHOT-shaded.jar
`
